package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.dto.model.UserDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IUserService {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @NotNull
    Boolean isLoginExists(@Nullable String login);

    @NotNull
    Boolean isEmailExists(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    @NotNull
    UserDTO removeOne(@Nullable UserDTO model);


    void clear();


    @NotNull List<UserDTO> findAll();


    @NotNull List<UserDTO> findAll(@Nullable Comparator<UserDTO> comparator);


    @Nullable UserDTO add(@Nullable UserDTO model);


    boolean existsById(@NotNull String id);


    @Nullable UserDTO findOneById(@NotNull String id);


    @Nullable UserDTO findOneByIndex(@Nullable Integer index);


    int getSize();


    @Nullable UserDTO remove(@Nullable UserDTO model);


    @Nullable UserDTO removeById(@NotNull String id);


    @Nullable UserDTO removeByIndex(@NotNull Integer index);


    void removeAll(@Nullable List<UserDTO> modelsRemove);


    @NotNull Collection<UserDTO> add(@NotNull Collection<UserDTO> models);


    @NotNull Collection<UserDTO> set(@NotNull Collection<UserDTO> models);


    void update(final @NotNull UserDTO object);


    @Nullable List<UserDTO> findAll(@Nullable Sort sort);
}
