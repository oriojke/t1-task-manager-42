package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.dto.model.SessionDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ISessionService {


    void clear(@Nullable String userId);


    @Nullable List<SessionDTO> findAll(@Nullable String userId);


    @Nullable List<SessionDTO> findAll(@Nullable String userId, @Nullable Comparator<SessionDTO> comparator);


    @Nullable SessionDTO add(@Nullable String userId, @NotNull SessionDTO model);


    @Nullable SessionDTO add(@NotNull SessionDTO model);


    boolean existsById(@Nullable String userId, @Nullable String id);


    boolean existsById(@Nullable String id);


    @Nullable SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable SessionDTO findOneById(@Nullable String id);


    @Nullable SessionDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);


    int getSize(@Nullable String userId);


    @Nullable SessionDTO remove(@Nullable String userId, @Nullable SessionDTO model);


    @Nullable SessionDTO remove(@Nullable SessionDTO model);


    @Nullable SessionDTO removeById(@Nullable String userId, @Nullable String id);


    @Nullable SessionDTO removeByIndex(@Nullable String userId, @Nullable Integer index);


    void removeAll(@Nullable String userId);


    @NotNull Collection<SessionDTO> add(@NotNull Collection<SessionDTO> models);


    @NotNull Collection<SessionDTO> set(@NotNull Collection<SessionDTO> models);


    @NotNull void update(final @NotNull SessionDTO object);


    @Nullable List<SessionDTO> findAll(@Nullable String userId, @Nullable Sort sort);


    @Nullable List<SessionDTO> findAll();
}
