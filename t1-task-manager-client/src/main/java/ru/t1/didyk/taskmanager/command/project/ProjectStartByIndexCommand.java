package ru.t1.didyk.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.ProjectStartByIndexRequest;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-index";
    @NotNull
    public static final String DESCRIPTION = "Start project by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(getToken());
        serviceLocator.getProjectEndpointClient().startByIndex(request);
    }

}
