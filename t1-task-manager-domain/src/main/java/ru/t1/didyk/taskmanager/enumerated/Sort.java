package ru.t1.didyk.taskmanager.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.comparator.CreatedComparator;
import ru.t1.didyk.taskmanager.comparator.NameComparator;
import ru.t1.didyk.taskmanager.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {
    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @Getter
    @NotNull
    private final String displayName;

    @Nullable
    private final Comparator<?> comparator;

    Sort(@NotNull final String displayName, @NotNull final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    @Nullable
    @SuppressWarnings("rawtypes")
    public Comparator getComparator() {
        return comparator;
    }

}
