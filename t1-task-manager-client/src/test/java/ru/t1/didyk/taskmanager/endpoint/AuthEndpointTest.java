package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.constant.SharedTestMethods;
import ru.t1.didyk.taskmanager.dto.request.UserLogoutRequest;
import ru.t1.didyk.taskmanager.dto.request.UserViewProfileRequest;
import ru.t1.didyk.taskmanager.dto.response.UserLogoutResponse;
import ru.t1.didyk.taskmanager.dto.response.UserViewProfileResponse;
import ru.t1.didyk.taskmanager.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public class AuthEndpointTest {

    @Test
    public void login() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        Assert.assertNotNull(userToken);
    }

    @Test
    public void logout() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull UserLogoutRequest request = new UserLogoutRequest(userToken);
        @Nullable UserLogoutResponse response = SharedTestMethods.getAuthEndpoint().logout(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void profile() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull UserViewProfileRequest request = new UserViewProfileRequest(userToken);
        @Nullable UserViewProfileResponse response = SharedTestMethods.getAuthEndpoint().profile(request);
        Assert.assertNotNull(response.getUser());
    }

}
