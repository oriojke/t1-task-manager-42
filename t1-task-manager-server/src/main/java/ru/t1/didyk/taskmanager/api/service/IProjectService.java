package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void clear(@Nullable String userId);

    @Nullable List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator<ProjectDTO> comparator);

    @Nullable ProjectDTO add(@Nullable String userId, @NotNull ProjectDTO model);

    @Nullable ProjectDTO add(@NotNull ProjectDTO model);

    boolean existsById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    @Nullable ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable ProjectDTO findOneById(@Nullable String id);

    @Nullable ProjectDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    @Nullable ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO model);

    @Nullable ProjectDTO remove(@Nullable ProjectDTO model);

    @Nullable ProjectDTO removeById(@Nullable String userId, @Nullable String id);

    @Nullable ProjectDTO removeByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

    @NotNull Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> models);

    @NotNull Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> models);

    @NotNull void update(final @NotNull ProjectDTO object);

    @Nullable List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable List<ProjectDTO> findAll();
}
