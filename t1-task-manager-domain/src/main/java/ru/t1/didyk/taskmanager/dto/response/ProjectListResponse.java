package ru.t1.didyk.taskmanager.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.ProjectDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResponse {

    @Nullable
    List<ProjectDTO> projects;

    public ProjectListResponse(@Nullable List<ProjectDTO> projects) {
        this.projects = projects;
    }
}
